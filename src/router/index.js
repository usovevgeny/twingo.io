import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/pages/Main'
import Friends from '@/pages/Friends'
import Travels from '@/pages/Travels'
import Travel from '@/pages/Travel'
import TravelCreate from '@/pages/TravelCreate'
import User from '@/pages/User'
import Registration from '@/pages/Registration'
import Settings from '@/pages/Settings'
import About from '@/pages/other_pages/About'
import Contacts from '@/pages/other_pages/Contacts'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/registration',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/friends',
      name: 'Friends',
      component: Friends
    },
    {
      path: '/travels',
      name: 'Travels',
      component: Travels
    },
    {
      path: '/travel/:id',
      name: 'Travel',
      component: Travel
    },
    {
      path: '/user/:id',
      name: 'User',
      component: User
    },
    {
      path: '/create',
      name: 'TravelCreate',
      component: TravelCreate
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts
    }
  ]
})
