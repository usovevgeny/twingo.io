// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource';
import TwingoAPI from './plugins/twingo.js'
import Auth from './plugins/auth.js'

Vue.use(VueResource);
Vue.use(Auth);
Vue.use(TwingoAPI, {
  api_url: process.env.API_URL,
  version: 1
});

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
