const Auth = {
  install (Vue) {
    Vue.prototype.$auth = {
      user: undefined,
      isAuth: false,
      set(token, user, remember) {
        localStorage['token'] = token;
        this.user = user;
        this.isAuth = true;
      },
      logout() {
        delete localStorage['token'];
        this.user = undefined;
      },
      getToken() {
        return localStorage['token'];
      },
      tokenExist() {
        return localStorage['token'] !== undefined;
      }
    }
  }
};

export default Auth;
