const TwingoAPI = {
  install (Vue, options) {
    Vue.prototype.$twingo = {
      getAvatarUploadUrl() {
        return `${options['api_url']}/v${options['version']}/account/change_avatar`;
      },
      call(method, params) {
        const api_url = `${options['api_url']}/v${options['version']}/${method}`;
        const headers = {
          'content-type': 'multipart/form-data'
        };

        if(localStorage['token']) {
          Vue.http.headers.common['Authorization'] = 'Token ' + localStorage['token'];
        }

        return Vue.http.post(api_url, params, {
          headers: headers,
          emulateJSON: true
        });
      }
    }
  }
};

export default TwingoAPI;
